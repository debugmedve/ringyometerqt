#include <QDir>
#include <QGuiApplication>
#include <QQmlEngine>
#include <QQmlContext>
#include <QQuickView>
#include <QQmlApplicationEngine>
#include <QQuickStyle>

#include "ringyorand.hpp"

int main(int argc, char *argv[]) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc,argv);
    app.setOrganizationName("Zoltan Medve");
    app.setOrganizationDomain("qt-project.org");
    app.setApplicationName(QFileInfo(app.applicationFilePath()).baseName());

    QQuickStyle::setStyle("material");

    QQmlApplicationEngine engine;
    RingyoRand ringyorand;
    engine.rootContext()->setContextProperty("ringyorand", &ringyorand);
    engine.load(QUrl("qrc:///ringyo.qml"));

    return app.exec();
}
