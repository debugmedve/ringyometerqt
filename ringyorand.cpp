#include "ringyorand.hpp"
#include <random>

using namespace std;

int RingyoRand::rrand() {
    return m_rand;
}

void RingyoRand::recalc() {
    std::random_device r;

    // Choose a random mean between 0 and 100
    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(0, 100);
    m_rand = uniform_dist(e1);

    emit randChanged();
}

