#ifndef RINGYORAND_HPP
#define RINGYORAND_HPP

#endif // RINGYORAND_HPP

#include <QObject>

class RingyoRand : public QObject {

Q_OBJECT
Q_PROPERTY(int rand READ rrand NOTIFY randChanged)

public:
    RingyoRand() : m_rand(0){}
int rrand();
Q_INVOKABLE void recalc();

signals:
void randChanged();

private:
int m_rand;

};
