import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1
import QtQuick.LocalStorage 2.0
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQuick.Layouts 1.2

ApplicationWindow {
    id: mainWindow
    title: qsTr("Ringy'O Meter QT")
    visible: true
    width: 500
    height: 500
    Material.theme: Material.Dark

    header: Pane {
        id: header
        contentWidth: parent.width

        ColumnLayout {
            width: parent.width

            Text {
                Layout.alignment: Qt.AlignLeft
                id: info
                text: qsTr("Ringy'O Meter")
                color: Material.color(Material.BlueGrey)
                font.family: "Helvetica"
                font.pointSize: 15
            } //text

            Rectangle {
                property color normal: "#0f0"
                property color warning: "orange"
                property color critical: "#f00"
                property int normalMaxValue: 50
                property int warningMaxValue: 80
                property int crtiicalMaxValue: 100

                id: led
                width: parent.width
                anchors.right: parent.right
                anchors.left: parent.left
                height: 2
                state: "NORMAL"
                color: normal

                states: [
                    State {

                        name: "NORMAL"
                        PropertyChanges {
                            target: led
                            color: led.normal
                        }
                    },
                    State {
                        name: "WARN"
                        PropertyChanges {
                            target: led
                            color: led.warning
                        }
                    },
                    State {
                        name: "CRITICAL"
                        PropertyChanges {
                            target: led
                            color: led.critical
                        }
                    }
                ]

                transitions: [
                    Transition {
                        to: "*"
                        ColorAnimation {
                            target: led
                            duration: 1000
                        }
                    }
                ]
            } //led
        } //layout
    } //header pane

    footer: Rectangle {
        Button {
            id: submit
            text: qsTr("Calculate")
            anchors.bottom: parent.bottom
            x: 20

            Material.background: Material.color(Material.Orange,
                                                Material.Shade700)
            onClicked: {
                ringyorand.recalc()
                led.state = (ringyorand.rand > led.warningMaxValue) ? "CRITICAL" : (ringyorand.rand > led.normalMaxValue) ? "WARN" : "NORMAL"
            }
        }
    }//footer

    Pane {
        id: content
        width: parent.width
        height: parent.height

        Pane {
            id: gaugepane
            width: parent.width - 20
            height: parent.height - 20
            anchors.centerIn: parent

            CircularGauge {
                id: gauge

                style: CircularGaugeStyle {
                    needle: Rectangle {
                        y: outerRadius * 0.15
                        implicitWidth: outerRadius * 0.03
                        implicitHeight: outerRadius * 0.9
                        antialiasing: true
                        color: Qt.rgba(0.66, 0.3, 0, 1)
                    }
                    tickmarkLabel: Text {
                        font.pixelSize: Math.max(6, outerRadius * 0.1)
                        text: styleData.value
                        color: styleData.value >= led.warningMaxValue ? led.critical : styleData.value >= led.normalMaxValue ? led.warning : led.normal
                        antialiasing: true
                    }
                    tickmark: Rectangle {
                        visible: styleData.value < 80
                                 || styleData.value % 10 == 0
                        implicitWidth: outerRadius * 0.02
                        antialiasing: true
                        implicitHeight: outerRadius * 0.06
                        color: styleData.value >= led.warningMaxValue ? led.critical : styleData.value >= led.normalMaxValue ? led.warning : led.normal
                    }
                    minorTickmark: Rectangle {
                        implicitWidth: outerRadius * 0.01
                        antialiasing: true
                        implicitHeight: outerRadius * 0.03
                        color: styleData.value >= led.warningMaxValue ? led.critical : styleData.value >= led.normalMaxValue ? led.warning : led.normal
                    }
                }

                Behavior on value {
                    NumberAnimation {
                        duration: 400
                    }
                }

                value: ringyorand.rand

                anchors.centerIn: parent
                anchors.fill: parent
            }//gauge
        }//gaugepane
    }
}//window
